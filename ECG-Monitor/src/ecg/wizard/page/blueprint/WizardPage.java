package ecg.wizard.page.blueprint;

import ecg.wizard.blueprint.Wizard;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.LabelBuilder;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

@SuppressWarnings("restriction")
/** basic wizard page class */
public abstract class WizardPage extends VBox {
	public Button priorButton = new Button("_Previous");
	public Button nextButton = new Button("N_ext");
	public Button cancelButton = new Button("Cancel");
	public Button finishButton = new Button("_Finish");

	public WizardPage(String title) {
		getChildren().add(
				LabelBuilder.create().text(title)
						.style("-fx-font-weight: bold; -fx-padding: 0 0 5 0;")
						.build());
		setId(title);
		setSpacing(5);
		setStyle("-fx-padding:10; -fx-background-color: honeydew; -fx-border-color: derive(honeydew, -30%); -fx-border-width: 3;");

		Region spring = new Region();
		VBox.setVgrow(spring, Priority.ALWAYS);
		getChildren().addAll(getContent(), spring, getButtons());

		priorButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent actionEvent) {
				priorPage();
			}
		});
		nextButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent actionEvent) {
				nextPage();
			}
		});
		cancelButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent actionEvent) {
				getWizard().cancel();
			}
		});
		finishButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent actionEvent) {
				getWizard().finish();
			}
		});
	}

	public HBox getButtons() {
		Region spring = new Region();
		HBox.setHgrow(spring, Priority.ALWAYS);
		HBox buttonBar = new HBox(5);
		cancelButton.setCancelButton(true);
		finishButton.setDefaultButton(true);
		buttonBar.getChildren().addAll(spring, priorButton, nextButton,
				cancelButton, finishButton);
		return buttonBar;
	}

	public abstract Parent getContent();

	public boolean hasNextPage() {
		return getWizard().hasNextPage();
	}

	public boolean hasPriorPage() {
		return getWizard().hasPriorPage();
	}

	public void nextPage() {
		getWizard().nextPage();
	}

	public void priorPage() {
		getWizard().priorPage();
	}

	public void navTo(String id) {
		getWizard().navTo(id);
	}

	public Wizard getWizard() {
		return (Wizard) getParent();
	}

	public void manageButtons() {
		if (!hasPriorPage()) {
			priorButton.setDisable(true);
		}

		if (!hasNextPage()) {
			nextButton.setDisable(true);
		}
	}
}
