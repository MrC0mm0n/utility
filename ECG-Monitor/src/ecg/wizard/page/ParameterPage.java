package ecg.wizard.page;

import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import ecg.wizard.page.blueprint.WizardPage;

@SuppressWarnings("restriction")
public class ParameterPage extends WizardPage {

	public ParameterPage() {
		super("Parameters");
	}

	@Override
	public Parent getContent() {

		Group root = new Group();

		// Controls to be added to the HBox
		Label label = new Label("Test Label:");
		TextField tb = new TextField();
		Button button = new Button("Button...");

		// HBox with spacing = 5
		HBox hbox = new HBox(5);
		hbox.getChildren().addAll(label, tb, button);
		hbox.setAlignment(Pos.CENTER);

		root.getChildren().add(hbox);

		return root;
	}

}
