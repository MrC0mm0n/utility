package ecg.wizard.page;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Parent;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBoxBuilder;
import ecg.wizard.page.blueprint.WizardPage;

@SuppressWarnings("restriction")
public class OptionSelectionPage extends WizardPage {

	private RadioButton option1;
	private RadioButton option2;
	private ToggleGroup options = new ToggleGroup();

	public OptionSelectionPage() {
		super("Please choose from the options below:");

		nextButton.setDisable(true);
		finishButton.setDisable(true);

		option1.setToggleGroup(options);
		option2.setToggleGroup(options);

		options.selectedToggleProperty().addListener(
				new ChangeListener<Toggle>() {
					@Override
					public void changed(
							ObservableValue<? extends Toggle> observableValue,
							Toggle oldToggle, Toggle newToggle) {
						nextButton.setDisable(false);
						finishButton.setDisable(false);
					}
				});
	}

	@Override
	public Parent getContent() {
		option1 = new RadioButton("Monitor Live ECG");
		option2 = new RadioButton("Parameters");

		return VBoxBuilder.create().spacing(5).children(option1, option2)
				.build();

	}

	public void nextPage() {

		if (options.getSelectedToggle().equals(option1)) {
			super.nextPage();
		} else if (options.getSelectedToggle().equals(option2)) {
			navTo("Parameters");
		}
	}

}
