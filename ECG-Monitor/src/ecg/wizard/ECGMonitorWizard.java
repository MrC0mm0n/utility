package ecg.wizard;

import javafx.stage.Stage;
import ecg.wizard.blueprint.Wizard;
import ecg.wizard.page.LiveECGPage;
import ecg.wizard.page.OptionSelectionPage;
import ecg.wizard.page.ParameterPage;

@SuppressWarnings("restriction")
public class ECGMonitorWizard extends Wizard {
	Stage owner;

	public ECGMonitorWizard(Stage owner) {
		// super(new ComplaintsPage(), new MoreInformationPage(), new
		// ThanksPage());
		super(new OptionSelectionPage(), new LiveECGPage(), new ParameterPage());
		this.owner = owner;
	}

	public void finish() {
		owner.close();
	}

	public void cancel() {
		System.out.println("Cancelled");
		owner.close();
	}
}