package ecg;

import ecg.wizard.ECGMonitorWizard;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

// changes for branch 2
/** This class displays a wizard */
@SuppressWarnings("restriction")
public class ECGMonitor extends Application {
	public static void main(String[] args) throws Exception {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		// configure and display the scene and stage.
		stage.setScene(new Scene(new ECGMonitorWizard(stage), 640, 480));
		stage.setTitle("Live ECG Monitoring");
		stage.show();
	}
}
