import java.util.Properties;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class MainController {

	private static final Log logger = LogFactory.getLog(MainController.class);

	final static String fromUser = "??????";
	final static String fromPass = "??????";
	final static String emailFolder = "inbox";

	final static String toUser = "??????";
	// final static String toPass = "??????";
	final static int sleep = 3000;

	public static void main(String[] args) {
		logger.info("Starting spamming app.");

		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(fromUser, fromPass);
					}
				});

		try {

			Store store = session.getStore("imaps");
			store.connect("imap.gmail.com", fromUser, fromPass);
			Folder[] folderArr = store.getDefaultFolder().list();
			for (Folder folder : folderArr) {
				// if (folder.getName().equals("Buffer")) {
				if (folder.getName().equals(emailFolder)) {
					logger.info("Entered Buffer folder");
					logger.info("Total messages in folder: "
							+ folder.getMessageCount());
					folder.open(Folder.READ_ONLY);

					int countEmailFwd = 1;
					for (int i = 1; i < folder.getMessageCount() + 1; i++) {
						Message message = folder.getMessage(i);
						Address[] add = message.getAllRecipients();

						if (add[0].toString().equals(toUser)) {

							// compose the message to forward
							Message messageFwd = new MimeMessage(session);
							messageFwd.setSubject("Fwd: "
									+ message.getSubject());
							messageFwd.setFrom(new InternetAddress(fromUser));
							messageFwd.addRecipient(Message.RecipientType.TO,
									new InternetAddress(toUser));

							// Create your new message part
							BodyPart messageBodyPart = new MimeBodyPart();
							messageBodyPart.setText("Oiginal message:\n\n");

							// Create a multi-part to combine the parts
							Multipart multipart = new MimeMultipart();
							multipart.addBodyPart(messageBodyPart);

							// Create and fill part for the forwarded content
							messageBodyPart = new MimeBodyPart();
							messageBodyPart.setDataHandler(message
									.getDataHandler());

							// Add part to multi part
							multipart.addBodyPart(messageBodyPart);

							// Associate multi-part with message
							messageFwd.setContent(multipart);

							// Send message
							Transport.send(messageFwd);

							logger.info(countEmailFwd
									+ " message forwarded ....");
							countEmailFwd++;

							logger.info("Sleeping for "
									+ Float.toString(sleep / 1000) + " sec...");
							Thread.sleep(sleep);

						}
					}
				}
			}

			logger.info("Closed email session");
		} catch (Exception e) {
			logger.info(e);
			// main(args);
		} finally {

		}

		logger.info("Closing spamming app.");
	}

}
